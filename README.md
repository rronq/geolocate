# Overview 

Geophone.py is an application for determining the general location of a phone number. This comes in both a console and GUI version. Some of the code was generated using chatGPT and pygubu as an experiment.

# Installing dependencies
The following dependencies can be installed system wide:
```
pip install phonenumbers
pip install tk
```


Use requirements.txt within a virtual environment:
```
pip -r requirements.txt
```
# Usage
For console, it will ask for your input. Input a number with the country code at the beginning:
```
11234567
```

For GUI, type the number in the input box and click locate. You must also put the **+** at the beginning of the number:
```
+11234567
```
