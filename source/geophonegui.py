import tkinter as tk
import phonenumbers
from phonenumbers import geocoder

class geolocate:
    def __init__(self, master=None):
        
        frame1 = tk.Frame(master)
        frame1.configure(height=75, width=200)
        
        self.userInput = tk.Entry()
        self.userInput.pack(side="top")
        
        self.Locate = tk.Button(text="Locate", command=self.geolocateNumber)
        self.Locate.pack(side="top")
        
        phoneLabel = tk.Label(text="Enter a phone number(Add call code):")
        phoneLabel.pack(side="top")
        
        self.geolocate_label = tk.Label(text="")
        self.geolocate_label.pack()
                
        frame1.pack(side="top")

        self.mainwindow = frame1
        

    def geolocateNumber(self):
        phoneNumber = "+" + self.userInput.get()
        try:
            parsedNum = phonenumbers.parse(phoneNumber)
            country = geocoder.country_name_for_number(parsedNum, "en")
            region = geocoder.description_for_number(parsedNum, "en")
            self.geolocate_label.config(text=f"Country: {country}\nRegion: {region}")
        except phonenumbers.NumberParseException as e:
            self.geolocate_label.config(text=f"Error: {e}")
    

    def run(self):
        self.mainwindow.mainloop()


if __name__ == "__main__":
    root = tk.Tk()
    root.title("Phone Number Geolocator")
    root.resizable(False, False)
    app = geolocate(root)
    app.run()
