import phonenumbers
from phonenumbers import geocoder

phoneNumber = input("Enter a phone number: ")
phoneNumber = '+' + phoneNumber.strip()

try:
    parsedNum = phonenumbers.parse(phoneNumber)
    country = geocoder.country_name_for_number(parsedNum, "en")
    region = geocoder.description_for_number(parsedNum, "en")

    print(f"Country: {country}\nRegion: {region}")
    
except phonenumbers.NumberParseException as e:
	print(f"Error: {e}")